import * as Router  from 'koa-router';
import { loginHandle, getHandle } from '../controller/user';

const router = new Router({ prefix: '/user' });

// TODO: move handler to controller
router.get('/', getHandle);


router.post('/login', loginHandle);

export default router;
