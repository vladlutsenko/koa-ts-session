import {newSession, newVist} from "../services/session";

export function sessionMiddleware(ctx, next) {
    const sessionId = ctx.cookies.get('sessionId');
    if (!sessionId) {
      const newSessionId=newSession();
      ctx.cookies.set('sessionId', newSessionId, { httpOnly: true });
    }
    else{
      newVist(sessionId);
    }
    next();
  }