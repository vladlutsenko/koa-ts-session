interface Session {
    sessionId: string;
    counter: number;
}
  
const sessions: Array<Session> = [];

export function newSession(){
    const sessionId = Date.now().toString();
    sessions.push({
        sessionId: sessionId,
        counter: 1
    })
    console.log(findSession(sessionId));
    return sessionId;
}

function findSession(sessionId: string){
    return sessions.find(session=>session.sessionId===sessionId);
}

export function newVist(sessionId: string){
    findSession(sessionId).counter++;
    console.log(findSession(sessionId));
}